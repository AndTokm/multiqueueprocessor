//============================================================================
// Name        : MultiQueueProcessor.cpp
// Created on  : 24.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : MultiQueueProcessor
//============================================================================

#define BOOST_TEST_MODULE "Asynch"

#include <iostream>
#include <map>
#include <vector>
#include <atomic>
#include <list>
#include <string>
#include <deque>
#include <complex>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <future>
#include <boost/test/included/unit_test.hpp>

/** IConsumer interface: **/
template<typename Key, typename Value>
class IConsumer {
public:
	virtual void Consume(Key id, const Value &value) = 0;
};


template<typename Key, typename Value>
class MultiQueueProcessor {
protected:
	// Queue consumers collection:
	std::map<Key, std::shared_ptr<IConsumer<Key, Value>>> consumers;

	// Messages storage. Containing a collection of messages separately
	// for each consumer.
	std::map<Key, std::deque<Value>> queues;

	// Queue processing worker thread:
	std::thread worker;

	// Threads synchronization primitive:
	std::mutex mtx;

	// Variable to indicate is some new messages added.
	std::condition_variable gotSome;

	// Switch to contol processing state:
	std::atomic_bool running{ true };

	// Consumer messages maximun number:
	inline static constexpr size_t max_capacity = 1000;

	// Timeout for waiting for events by condition variable:
	inline static constexpr size_t wait_cv_timeout_msec = 5000;

	// Timeout duration constant:
	inline static constexpr auto cv_timeout =
		std::chrono::milliseconds(MultiQueueProcessor::wait_cv_timeout_msec);

public:
	MultiQueueProcessor() {
		worker = std::thread(std::bind(&MultiQueueProcessor::Process, this));
	}

	virtual ~MultiQueueProcessor() noexcept {
		StopProcessing();
		worker.join();
	}

	// Stop queue processing/
	void StopProcessing() {
		running = false;
		// Notify handlers that we've got some ne messages:
		gotSome.notify_all();
	}

	// Returns the processing state:
	inline bool isRunning() const noexcept {
		return this->running;
	}

	// Removes an existing subscriber
	template<typename... Args>
	void Subscribe(Args&&... arguments) {
		std::unique_lock<std::mutex> lock{ mtx };
		consumers.emplace(std::forward<Args>(arguments)...);
	}

	// Describes a consumer from a queue to receive messages.
	// Availability check of an existing subscription is not performed
	void Unsubscribe(const Key& id) {
		std::unique_lock<std::mutex> lock{ mtx };
		consumers.erase(id);
		// TODO: May be delete all messages for this subsctiber?
	}

	// Push a message for a specific consumer by key.
	void PushMessage(const Key& id, Value&& value) {
		std::unique_lock<std::mutex> lock{ mtx };

		// std::queue<Value> will constructed only in case if no 
		// record with 'Key' exists:
		auto[record, ok] = queues.try_emplace(id);
		if (MultiQueueProcessor::max_capacity > record->second.size())
			record->second.emplace_back(std::forward<Value>(value));

		// Notify handlers that we've got some ne messages:
		gotSome.notify_all();
	}

	// Emplace a message for a specific consumer by key.
	// Note: Parameters are passed to the internal queue by means of a
	// perfect forwarding, thereby avoiding the overhead of copying and moving
	template <class... Args>
	void EmplaceMessage(Key& id, Args&&... args) {
		__emplace_message(std::move(id), std::forward<Args>(args)...);
	}

	// Emplace a message for a specific consumer by key.
	// Note: Parameters are passed to the internal queue by means of a
	// perfect forwarding, thereby avoiding the overhead of copying and moving
	template <class... Args>
	void EmplaceMessage(Key&& id, Args&&... args) {
		__emplace_message(std::move(id), std::forward<Args>(args)...);
	}

	[[deprecated]] /* TODO: No suggested to use due to performance impact */
	[[nodiscard]] Value Dequeue(const Key& id) {
		std::unique_lock<std::mutex> lock{ mtx };
		if (auto iter = queues.find(id); queues.end() != iter) {
			if (false == iter->second.empty()) {
				auto front = std::move(iter->second.front());
				iter->second.pop_front();
				return std::move(front);
			}
		}
		return Value{};
	}

protected:
	// Method: Protected __emplace_message method implementation.
	//         Shall be called by high level function templates.
	// Emplace a message for a specific consumer by key.
	// Note: Parameters are passed to the internal queue by means of a
	// perfect forwarding, thereby avoiding the overhead of copying and moving
	template <class... Args>
	void __emplace_message(Key&& id, Args&&... args) {
		std::unique_lock<std::mutex> lock{ mtx };

		// std::queue<Value> will constructed only in case if 
		// no record with 'Key' exists:
		auto[record, ok] = queues.try_emplace(id);
		if (MultiQueueProcessor::max_capacity > record->second.size())
			record->second.emplace_back(std::forward<Args>(args)...);

		// Notify handlers that we've got some ne messages:
		gotSome.notify_all();
	}



	void Process() {
		while (running) {
			// Acquire unique_lock on mutex:
			std::unique_lock<std::mutex> lock{ mtx };
			while (gotSome.wait_for(lock, cv_timeout) == std::cv_status::timeout) {
				if (false == running) {
					return; // Good bye.
				}
			}

			for (auto iter = queues.begin(); iter != queues.end(); ++iter) {
				if (iter->second.empty()) {
					// There is no need to search for consumers in the tree if 
					// there are still no messages for the consumer
					continue;
				}
				if (auto consumerIter = consumers.find(iter->first);
					consumers.end() != consumerIter) {
					// Pass data to the specific consumer:
					consumerIter->second->Consume(iter->first, iter->second.front());
					// Pop first message:
					iter->second.pop_front();
				}
			}
		}
	}
};

namespace UnitTests {

	template<typename Key, typename Value>
	class TestConsumer : public IConsumer<Key, Value> {
	private:
		size_t count = 0;

	public:
		virtual void Consume(Key id, const Value &value) override {
			count++;
		}

		size_t getMessagesCount() const noexcept {
			return count;
		}
	};

	using Message = std::string;
}

BOOST_AUTO_TEST_CASE(StartProcessing) {
	MultiQueueProcessor<int, UnitTests::Message> queue;
	BOOST_TEST(true == queue.isRunning());
}

BOOST_AUTO_TEST_CASE(Shutdown) {
	MultiQueueProcessor<int, UnitTests::Message> queue;
	BOOST_TEST(true == queue.isRunning());

	queue.StopProcessing();
	BOOST_TEST(false == queue.isRunning());
}

// TODO: Flaky!!! Don't meant to work
BOOST_AUTO_TEST_CASE(HandleMessages_OneThread) {
	using namespace UnitTests;
	MultiQueueProcessor<int, Message> queue;

	std::shared_ptr<TestConsumer<int, Message>> consumer = std::make_shared<TestConsumer<int, Message>>();
	queue.Subscribe(1, consumer);

	// Push messsage
	queue.EmplaceMessage(1, "Some message 1");

	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	BOOST_TEST(1 == consumer->getMessagesCount());
}


BOOST_AUTO_TEST_CASE(HandleMessages_Parralel_SingleConsumer) {
	using namespace UnitTests;
	MultiQueueProcessor<int, Message> queue;
	std::shared_ptr<TestConsumer<int, Message>> consumer = std::make_shared<TestConsumer<int, Message>>();
	int messages_to_send = 10;

	std::future<void> subscribe = std::async(std::launch::async, [&queue, consumer]()-> void {
		queue.Subscribe(1, consumer);
	});

	std::future<void> produce = std::async(std::launch::async, [&queue](int count, int timeout)-> void {
		for (int i = 0; i < count; i++) {
			std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
			queue.EmplaceMessage(1, "Some message " + std::to_string(i));
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
	}, messages_to_send, 10 /* Timeout */);

	subscribe.wait();
	produce.wait();
	BOOST_TEST(messages_to_send == consumer->getMessagesCount());
}


BOOST_AUTO_TEST_CASE(HandleMessages_Parralel_TwoConsumers) {
	using namespace UnitTests;
	MultiQueueProcessor<int, Message> queue;

	std::shared_ptr<TestConsumer<int, Message>> consumer1 = std::make_shared<TestConsumer<int, Message>>();
	std::shared_ptr<TestConsumer<int, Message>> consumer2 = std::make_shared<TestConsumer<int, Message>>();
	int messages_to_send = 10;

	std::future<void> subscribe = std::async(std::launch::async, [&queue, consumer1, consumer2]()-> void {
		queue.Subscribe(1, consumer1);
		queue.Subscribe(2, consumer2);
	});

	std::future<void> produce = std::async(std::launch::async, [&queue](int count, int timeout)-> void {
		for (int i = 0; i < count; i++) {
			std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
			queue.EmplaceMessage(1, "Some message " + std::to_string(i));
			queue.EmplaceMessage(2, "Some message " + std::to_string(i));
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
	}, messages_to_send, 10 /* Timeout */);

	subscribe.wait();
	produce.wait();
	BOOST_TEST(messages_to_send == consumer1->getMessagesCount());
	BOOST_TEST(messages_to_send == consumer2->getMessagesCount());
}

BOOST_AUTO_TEST_CASE(HandleMessages_Parralel_10M_Messages) {
	using namespace UnitTests;
	MultiQueueProcessor<size_t, Message> queue;

	// 10000 messages to send for each of 1000 different consumers:
	size_t messages_to_send = 10000, consumers_count = 1000;

	std::vector<std::shared_ptr<TestConsumer<size_t, Message>>> conumers;
	conumers.reserve(consumers_count);

	for (size_t i = 0; i < consumers_count; i++)
		conumers.push_back(std::make_shared<TestConsumer<size_t, Message>>());

	// Subsribe all consumers:
	std::future<void> subscribe = std::async(std::launch::async, [&]()-> void {
		for (size_t i = 0; i < consumers_count; i++)
			queue.Subscribe(i, conumers[i]);
	});

	std::future<void> produce = std::async(std::launch::async, [&](size_t count, int timeout)-> void {
		for (size_t i = 0; i < count; i++) {
			std::this_thread::sleep_for(std::chrono::nanoseconds(timeout));
			for (size_t id = 0; id < consumers_count; id++)
				queue.EmplaceMessage(id, "Some message " + std::to_string(i));
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
	}, messages_to_send, 1 /* Timeout */);

	subscribe.wait();
	produce.wait();

	for (size_t i = 0; i < consumers_count; i++)
		BOOST_TEST(messages_to_send == conumers[i]->getMessagesCount());
}
