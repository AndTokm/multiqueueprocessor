# MultiQueueProcessor

Asynchronous queue implementation

**Requirements**: C ++ 17, Boost (For tests only)

*Launch example:*
> Queue.exe --log_level = all

```
Running 6 test cases...
Entering test module "Asynch"
Entering test case "StartProcessing"
info: check true == queue.isRunning() has passed
Leaving test case "StartProcessing"; testing time: 3973us
Entering test case "Shutdown"
info: check true == queue.isRunning() has passed
info: check false == queue.isRunning() has passed
Leaving test case "Shutdown"; testing time: 3365us
Entering test case "HandleMessages_OneThread"
info: check 1 == consumer->getMessagesCount() has passed
Leaving test case "HandleMessages_OneThread"; testing time: 101025us
Entering test case "HandleMessages_Parralel_SingleConsumer"
info: check messages_to_send == consumer->getMessagesCount() has passed
Leaving test case "HandleMessages_Parralel_SingleConsumer"; testing time: 111899us
Entering test case "HandleMessages_Parralel_TwoConsumers"
info: check messages_to_send == consumer1->getMessagesCount() has passed
info: check messages_to_send == consumer2->getMessagesCount() has passed
Leaving test case "HandleMessages_Parralel_TwoConsumers"; testing time: 111278us
Entering test case "HandleMessages_Parralel_10M_Messages"
........
info: check messages_to_send == conumers[i]->getMessagesCount() has passed
Leaving test case "HandleMessages_Parralel_10K_Messages"; testing time: 38564655us
Leaving test module "Asynch"; testing time: 38922428us

*** No errors detected
```
